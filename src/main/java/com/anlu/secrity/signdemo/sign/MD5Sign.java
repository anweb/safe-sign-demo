package com.anlu.secrity.signdemo.sign;

import org.springframework.util.DigestUtils;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;

import javax.servlet.http.HttpServletRequest;


import javax.servlet.http.HttpServletRequest;

/**
 * @description:
 * @author: Andrew
 * @create: 2019-03-15 19:25
 **/
public class MD5Sign {
    /**
     * 方法描述:将字符串MD5加码 生成32位md5码
     *
     * @author leon 2016年10月10日 下午3:02:30
     * @param inStr
     * @return
     */
    public static String md5(String inStr) {
        try {
            String encodeMd5= DigestUtils.md5DigestAsHex(inStr.getBytes("UTF-8"));
            return encodeMd5;
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("MD5签名过程中出现错误");
        }
    }

    /**
     * 方法描述:签名字符串
     *
     * @author leon 2016年10月10日 下午2:54:47
     * @param params 需要签名的参数
     * @param appSecret 签名密钥
     * @return
     */
    public static String sign(Map<String, Object> params, String appSecret) {
        StringBuilder valueSb = new StringBuilder();
        params.put("appSecret", appSecret);
        // 将参数以参数名的字典升序排序
        Map<String, Object> sortParams = new TreeMap<String, Object>(params);
        Set<Entry<String, Object>> entrys = sortParams.entrySet();
        // 遍历排序的字典,并拼接value1+value2......格式
        for (Entry<String, Object> entry : entrys) {
            valueSb.append(entry.getValue());
        }
        params.remove("appSecret");
        return md5(valueSb.toString());
    }

    /**
     * 方法描述:验证签名
     *
     * HttpServletRequest request 之前是request 请求
     *
     * @author leon 2016年10月10日 下午2:31:23
     * @param
     * @param appSecret 加密秘钥
     * @param paramsMap
     * @return
     * @throws Exception
     */
    public static boolean verify(String appSecret, HashMap<String, Object> paramsMap) throws Exception {

        String sign = (String) paramsMap.get("sign");
        if (sign == null) {
            throw new Exception(URLEncoder.encode("请求中没有带签名", "UTF-8"));
        }
//        if (request.getParameter("timestamp") == null) {
//            throw new Exception(URLEncoder.encode("请求中没有带时间戳", "UTF-8"));
//        }

        HashMap<String, String> params = new HashMap<String, String>();

        // 获取url参数
        @SuppressWarnings("unchecked")
        Enumeration<String> enu = (Enumeration<String>) paramsMap.keySet();
        while (enu.hasMoreElements()) {
            String paramName = enu.nextElement().trim();
            if (!paramName.equals("sign")) {
                // 拼接参数值字符串并进行utf-8解码，防止中文乱码产生
                params.put(paramName, URLDecoder.decode((String)paramsMap.get(paramName), "UTF-8"));
            }
        }

        params.put("appSecret", appSecret);

        // 将参数以参数名的字典升序排序
        Map<String, String> sortParams = new TreeMap<String, String>(params);
        Set<Entry<String, String>> entrys = sortParams.entrySet();

        // 遍历排序的字典,并拼接value1+value2......格式
        StringBuilder values = new StringBuilder();
        for (Entry<String, String> entry : entrys) {
            values.append(entry.getValue());
        }

        String mysign = md5(values.toString());
        if (mysign.equals(sign)) {
            return true;
        } else {
            return false;
        }

    }

    public static void main(String[] args)throws Exception {
        HashMap<String, Object> map = new HashMap<String, Object>();
        map.put("name","admin");
        map.put("age","12");


        String result=MD5Sign.sign(map,"121212");
        System.out.println(result);

        boolean flag= MD5Sign.verify("121212",map);

        System.out.println(flag);
    }


}
